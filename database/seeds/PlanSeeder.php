<?php

use Illuminate\Database\Seeder;
use App\Models\Plans;
class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name' => 'Front-End',
                'unique_id' => '10234'
            ],
            [
                'name' => 'Agency',
                'unique_id' => '20585'
            ],
            [
                'name' => 'Reseller',
                'unique_id' => '22552'
            ],
        ];

            foreach($plans as $plan){
                Plans::create($plan);
            }
    }
}
