<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('assets/login/bootstrap.min.css')}}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{asset('assets/login/animate.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('assets/login/style.css')}}" rel="stylesheet">


</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="new-login-register">
        <div class="lg-info-panel">
            <div class="inner-panel">

                <div class="lg-content">
                    <a href="javascript:void(0)" class="p-20 di"><img
                            src="{{asset('assets/images/main_logo2.png')}}" width="300"
                            height="100"></a>

                </div>
            </div>
        </div>
        <div class="new-login-box">
            <div class="white-box">
                <div class="print-message-box" id="message">
                    @foreach($errors->all() as $error)

                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <center>{{$error}}</center>
                    </div>

                    @endforeach
                    <?php Session::forget('red'); Session::forget('green');  ?>

                </div>
                <h3 class="box-title m-b-0">Sign In</h3>
                <small>Enter your details below</small>
                <form class="form-horizontal new-lg-form" method="POST" action="{{route('login')}}" autocomplete="off">

                    <div class="form-group  m-t-20">
                        <div class="col-xs-12">
                            <label>Email Address</label>
                            <input class="form-control" type="text" required name="email" value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Password</label>
                            <input class="form-control" type="password" required name="password" value="{{old('password')}}">
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button
                                class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light"
                                type="submit">Log In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </section>
    <!-- jQuery -->
    <script src="{{asset('assets/login/jquery.min.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('assets/login/bootstrap.min.js')}}"></script>

    <script src="{{asset('assets/login/custom.min.js')}}"></script>
</body>

</html>
