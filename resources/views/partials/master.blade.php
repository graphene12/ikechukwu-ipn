@include('partials.head')

<body>
    <div class="container-scroller">

        @include('partials.navbar')

        <div class="container-fluid page-body-wrapper">
            
            @include('partials.sidebar')
            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('contents')
                </div>
                @include('partials.footer')
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    @include('partials.scripts')
</body>

</html>
