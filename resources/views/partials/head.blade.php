<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{config('app.name')}} - {{$title}}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <style>
        .sidebar .nav .nav-item.active>.nav-link .menu-title {
            color: #f7941c;
        }

        .sidebar .nav .nav-item.active>.nav-link i {
            color: #f7941c;
        }

        .sidebar .nav.sub-menu .nav-item .nav-link.active {
            color: #f7941c;
            background: transparent;
        }

        .text-primary,
        .list-wrapper .completed .remove {
            color: #f7941c !important;
        }
    </style>
</head>
