@extends('partials.master')
@section('contents')
<div class="row">
    <div class="col-md-8 offset-md-2 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Registration Link</h4>
          <div class="row mt-3">
            <div class="col-md-6 offset-md-3 pr-1" style="text-align: center;">
              <img src="assets/images/dashboard/img_1.jpg" class="mb-2 mw-100 w-100 rounded" alt="image">
              <a href="#" class="btn btn-success mx-auto" >Register Here</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
