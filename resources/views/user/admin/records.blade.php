@extends('partials.master')
@section('contents')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Records</h3>
    </div>
    <div class="row">
        <div class="col-lg-8 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Plan</th>
                                <th>Transaction Type</th>
                                <th>Affliate</th>
                                <th>Amount</th>
                                <th>Payment Method</th>
                                <th>JvZoo Payment ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($records as $record)
                            <tr>
                                <td>{{$record->user->name}}</td>
                                <td>{{$record->plan->name}}</td>
                                <td>{{$record->transaction_type}}</td>
                                <td>{{$record->affliate}}</td>
                                <td>{{$record->amount}}</td>
                                <td>{{$record->payment_method}}</td>
                                <td>{{$record->jvzoo_payment_id}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
