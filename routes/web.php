<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@login')->name('landing_page');
Route::post('ipn_register', 'LoadController@checkEmail');
Route::post('login', 'LoadController@authenticate')->name('login');

Route::group(['prefix' => 'user', 'middleware' => 'user'], function () {
    Route::get('/', 'PageController@home')->name('user_home');
    Route::get('logout', 'LoadController@logout')->name('logout');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('users','PageController@users')->name('users');
        Route::get('records','PageController@records')->name('records');
     });
});
