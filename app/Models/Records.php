<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Records extends Model
{
    protected $table = 'records';

    protected $fillable = [
        'plan_id','user_id','transaction_type','affliate','amount','payment_method','jvzoo_payment_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function plan(){
        return $this->belongsTo(Plans::class);
    }
}
