<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $plan = 'plans';
    protected $fillable = [
        'name','unique_id'
    ];
}
