<?php

namespace App\Http\Controllers;

use App\Repositories\User\UserRepository;
use App\Models\Plans;
use App\Models\User;
use App\Models\Records;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Hash;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Mail\Appreciation;

class LoadController extends Controller
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function validateData(array $data)
    {
        return Validator::make($data, [
            'ccustname' => 'required|string',
            'ccustemail' => 'required|email|unique:users,email',
            'cproditem' => 'required|integer|exists:plans,unique_id',
            'ctransaction' => 'required',
            'ctransaffiliate' => 'required',
            'ctransamount' => 'required',
            'ctranspaymentmethod' => 'required',
            'ctransreceipt' => 'required'
        ]);
    }

    public function returnPlan($plan_unique_id)
    {
        $plan = Plans::where('unique_id', $plan_unique_id)->first();
        return $plan->id;
    }

    public function registration(Request $request)
    {

        $data = $request->all();
        $validation = $this->validateData($data);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 300);
        } else {
            $plan = $this->returnPlan($request['cproditem']);

            if (!empty($plan)) {
                $password = Str::random(10);
                $userdata = [
                    'name' => ucwords(strtolower(trim($request['ccustname']))),
                    'email' => $request['ccustemail'],
                    'password' => Hash::make($password),
                    'plan_id' => $plan

                ];

                $success = $this->user->create($userdata);

                $record = [
                    'user_id' => $success->id,
                    'plan_id' => $plan,
                    'transaction_type' => $request['ctransaction'],
                    'affliate' => $request['ctransaffiliate'],
                    'amount' => $request['ctransamount'],
                    'payment_method' => $request['ctranspaymentmethod'],
                    'jvzoo_payment_id' => $request['ctransreceipt'],

                ];

                if ($success) {

                    $this->createRecord($record);
                    Mail::to($data['ccustemail'])->send(new Appreciation($data['ccustemail'],$password));
                    return response()->json([
                        'message' => 'Registration Successful. This Login Credentials will be sent to your email address. ',
                        'login_credentials' => [
                            'email' => $userdata['email'],
                            'password' => $password
                        ]
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Registration not Successful'
                    ], 300);
                }
            } else {
                return response()->json(['error' => 'Invalid Product'], 300);
            }
        }
    }

    public function checkEmail(Request $request)
    {
        $email = $request['ccustemail'];
        $user = User::where('email', $email)->first();

        if ($user) {
                $record = [
                    'user_id' => $user->id,
                    'plan_id' => $user->plan_id,
                    'transaction_type' => $request['ctransaction'],
                    'affliate' => $request['ctransaffiliate'],
                    'amount' => $request['ctransamount'],
                    'payment_method' => $request['ctranspaymentmethod'],
                    'jvzoo_payment_id' => $request['ctransreceipt'],

                ];
                $this->createRecord($record);
                return response()->json(['message' => 'Record Taken'], 200);

        } else {
            return $this->registration($request);
        }
    }


    public function createRecord(array $data)
    {
        return Records::create($data);
    }

    public function authenticate(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'email' => ['required', 'email', 'exists:users,email'],
            'password' => ['required']
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']], 1)) {
                return redirect()->route('user_home');
            } else {
                return redirect()->back()->withErrors(['password' => "Your password is Invalid"])->withInput();
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('landing_page');
    }
}
