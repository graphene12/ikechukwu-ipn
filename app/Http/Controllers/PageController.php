<?php

namespace App\Http\Controllers;

use App\Models\Records;
use Auth;
use Illuminate\Http\Request;
use App\Models\User;

class PageController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route('user_home');
        } else {
            return view('login');
        }
    }

    public function home(Request $request)
    {
        return view('user.home', ['title' => 'Home']);
    }

    public function users(Request $request)
    {
        $users = User::all();
        return view('user.admin.users', ['title' => 'Users', 'users' => $users]);
    }

    public function records(Request $request)
    {
        $records = Records::all();
        return view('user.admin.records', ['title' => 'Records', 'records' => $records]);
    }
}
