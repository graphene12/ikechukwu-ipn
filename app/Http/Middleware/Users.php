<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Users
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('landing_page')->withErrors(['message' => 'Unauthorised. Please Login']);
        }
        return $next($request);
    }
}
